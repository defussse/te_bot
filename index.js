const login = require('facebook-chat-api');
const fs = require('fs');
const path = require('path');
const fmbr = require('./utils/utils.js');
var _ = require('lodash');

login({ appState : JSON.parse(fs.readFileSync('appstate.json', 'utf8')) }, (err, api) => {
    if(err)
        return console.error(err);

    api.setOptions({ selfListen : false, logLevel : "silent" });

    api.listen((err, message) => {
        if(err)
            return console.error(err)
            switch(message.body)
            {
            case 'ping':
                api.sendMessage('pong', message.threadID);
                break;
            case 'blacklist':
                let blacklistlib = JSON.parse(fs.readFileSync('./kick/badword.json'));
                api.sendMessage(JSON.stringify(blacklistlib), message.threadID);
                break;
            case "test":
                api.sendMessage({ sticker : 387545608037990 }, message.threadID);
            default:
                api.markAsRead(message.threadID);
                break;
            }
        function viewLog()
        {
            console.log('-------------------------\nmsg\n' + message.body + '\n-------------------------\nUID\n' +
                message.senderID + '\n-------------------------');
        }

        /*add user with ID*/
        if(message.body.indexOf('add ') == 0) {
            var ID;
            api.addUserToGroup(ID = message.body.slice(4, message.body.length), message.threadID, (err))
            console.log(ID)
        }
        /*change thread ID*/
        if(message.body.indexOf('changeE ') == 0) {
            var emoji;
            api.changeThreadEmoji(emoji = message.body.slice(8, message.body.length), message.threadID, (err))
        }
        /*remove user with ID*/
        if(message.body.indexOf('kick ') == 0) {
            var ID;
            api.removeUserFromGroup(ID = message.body.slice(5, message.body.length), message.threadID, (err))
            console.log(ID)
        }
        /* check badword */
        if(message.body.indexOf('!!') == 0 || message.body.indexOf('del ') == 0) {
            // nothing
        } else {
            let badWords = JSON.parse(fs.readFileSync('./kick/badword.json', 'utf-8'));
            badWords.forEach(function(element) {
                if(message.body.indexOf(element) > -1) {
                    if(message.senderID != 100026013644825 || message.senderID != 100009854073587) {
                        api.removeUserFromGroup(message.senderID, message.threadID, (err));
                        console.log(message.body + ": Banned");
                    }
                }
            });
        }

        /*send sticker*/
        if(message.type == "message") {
            let stk = JSON.parse(fs.readFileSync('./stkscfg/cfg.json'));
            stk.forEach(element => {
                if(element == message.body) {
                    fs.readdir(path.join(__dirname, 'data', 'sticker'), (err, files) => {
                        let ran = Math.floor(Math.random() * parseInt(files.length));
                        return Promise
                            .resolve({
                                attachment : fs.createReadStream(path.join(__dirname, 'data', 'sticker', files[ran]))
                            })
                            .then(a => api.sendMessage(a, message.threadID));
                    });
                }
            });
        }

        //----
        if(message.body.indexOf('sticker -> ') == 0) {
            let emoji = message.body.slice(11, message.body.length);
            let inputDat = JSON.parse(fs.readFileSync('./stkscfg/cfg.json'));
            let wordArr = fmbr.toArray(inputDat)
            if(fmbr.exist(emoji, wordArr))
            {
                // Avoid
            }
            else
            {
                fmbr.saveArrayToFile(fmbr.addToArrayIfNeeded(emoji, wordArr), './stkscfg/cfg.json');
            }
        }

        /*add to blacklist*/
        if(message.body.indexOf('!! ') == 0) {

            let word = message.body.slice(3, message.body.length);
            let oldDat = JSON.parse(fs.readFileSync('./kick/badword.json'));
            let wordArr = fmbr.toArray(oldDat);
            if(fmbr.exist(word, wordArr)) {
                // avoid
            } else if(message.body.slice(3, message.body.length) != 'blacklist') {
                fmbr.saveArrayToFile(fmbr.addToArrayIfNeeded(word, wordArr), './kick/badword.json');
            } else if(word == 'blacklist') {
                api.sendMessage('Điên à', message.threadID);
            }
        }
        /*delete from blacklist*/
        if(message.body.indexOf('del ') == 0) {
            let word = message.body.slice(4, message.body.length);
            let oldDat = JSON.parse(fs.readFileSync('./kick/badword.json'));
            console.log(oldDat);
            let wordArr = fmbr.toArray(oldDat);

            let outputArray = wordArr.filter(function(element) {
                return element !== word
            });
            console.log(outputArray)
            fmbr.saveArrayToFile(outputArray, './kick/badword.json');
        }
        /*say*/
        if(message.body.indexOf("say ") == 0) {
            const say = require("./Say/say.js");
            say(message.body.slice(4, message.body.length), function() {
                let m = { body : "", attachment : fs.createReadStream(__dirname + '/Say/src/say.mp3') }; api.sendMessage(
                    m, message.threadID);
            });
        }
        /*chúc bé ngủ ngon =))*/
        if(message.body == 'chúc bé ngủ ngon' || message.body == 'g9') {
            let m = { body : "", attachment : fs.createReadStream(__dirname + '/event/g9.mp3') }; api.sendMessage(
                m, message.threadID);
        }

        /*line downloader*/
        if(message.senderID == '100009854073587') {
            if(message.body.indexOf("lineStore ") == 0) {
                let line = require("./LineDownloader/line_downloader.js");
                let url = message.body.slice(10, message.body.length);
                if(url != "")
                    line.get(url);
            }
        } else {
            console.log('ERR');
        }

        if(message.type == 'message') {
            if(fs.existsSync('./usr/' + message.senderID + '.json')) {
                let dat = JSON.parse(fs.readFileSync('./usr/' + message.senderID + '.json'));
                let scr = parseInt(dat.score);
                scr += 1;
                let level = Math.floor(scr / 150);
                let newData = { score : scr, level : level }; fs.writeFileSync(
                    './usr/' + message.senderID + '.json', JSON.stringify(newData), 'utf-8');
            } else {
                fmbr.createNewFile('./usr/' + message.senderID + '.json');
                let obj = { score : 0, level : 0 }; fs.writeFileSync(
                    './usr/' + message.senderID + '.json', JSON.stringify(obj), 'utf-8');
            }
        }

        if(message.body === "check rank") {
            if(fs.existsSync('./usr/' + message.senderID + '.json')) {
                let woa = JSON.parse(fs.readFileSync('./usr/' + message.senderID + '.json'));
                api.sendMessage('Score: ' + woa.score + '\nLevel: ' + woa.level, message.threadID);
            } else {
                fmbr.createNewFile('./usr/' + message.senderID + '.json');
                let obj = { score : 0, level : 0 }; fs.writeFileSync(
                    './usr/' + message.senderID + '.json', JSON.stringify(obj), 'utf-8');
            }
        }
        if(message.body.indexOf('->') != -1) {
            let status;
            let Arr1 = JSON.parse(fs.readFileSync('./FLATDB/MTR.json'));
            let Arr2 = message.body.split('->', 2);
            let newObj = { message : Arr2[0], reply : Arr2[1] };
            Arr1.push(newObj);
            fs.writeFileSync('./FLATDB/MTR.json', JSON.stringify(Arr1), 'utf-8');
        }

        if(message.type == 'message') {
            let procc = JSON.parse(fs.readFileSync('./FLATDB/MTR.json'));
            procc.forEach(element => {
                if(message.body == element.message) {
                    api.sendMessage(element.reply, message.threadID);
                }
            });
            let proc = JSON.parse(fs.readFileSync("./FLATDB/MTS.json"));
            proc.forEach(element => {
                if(message.body == element.message) {
                    api.sendMessage({ sticker : element.sticker }, message.threadID);
                }
            });
        }
        // Del message from DB
        if(message.body.indexOf('del ') != -1) {
            let word = message.body.slice(4, message.body.length);
            let ODat = JSON.parse(fs.readFileSync('./FLATDB/MTR.json'));
            let outputArray = ODat.filter(function(e) {
                return e.message !== word
            });
            console.log(outputArray);
            fs.writeFileSync('./FLATDB/MTR.json', JSON.stringify(outputArray), "utf-8");

            let oldDAt = JSON.parse(fs.readFileSync("./FLATDB/MTS.json"));
            let outputCft = oldDAt.filter(function(e) {
                return e.message !== word
            });
            console.log(outputArray);
            fs.writeFileSync('./FLATDB/MTS.json', JSON.stringify(outputCft), "utf-8");
        }
        /*ERR, use with caution*/
        if(message.body.indexOf("def ") != -1) {
            api.sendMessage("Gửi một nhãn dán", message.threadID);
        }

        if(message.type == "message") {
            if(message.attachments != null && message.attachments != "") {
                if(fs.existsSync("./data/LastMSG/commandEx/" + message.senderID + ".txt")) {
                    let dataEx = fs.readFileSync("./data/LastMSG/commandEx/" + message.senderID + ".txt");
                    if(message.attachments[0].type == "sticker" && dataEx.indexOf("def ") != -1) {
                        let Arr1 = JSON.parse(fs.readFileSync('./FLATDB/MTS.json'));
                        let Msg = dataEx.slice(4, dataEx.length);
                        let StkID = message.attachments[0].ID;
                        console.log("dasdasdasdsad:" + Msg);
                        let obj = {
                            message : Msg,
                            sticker : StkID,
                        } 
                        console.log("OBJECT:" + JSON.stringify(obj));
                        Arr1.push(obj);
                        fs.writeFileSync('./FLATDB/MTS.json', JSON.stringify(Arr1), 'utf-8');
                    }
                }
            }
        }

        if(message.body.indexOf("def ") != -1) {
            if(fs.existsSync("./data/LastMSG/commandEx/" + message.senderID + ".txt")) {
                fs.writeFileSync("./data/LastMSG/commandEx/" + message.senderID + ".txt", message.body, "utf-8");
            } else {
                fmbr.createNewFile("./data/LastMSG/commandEx/" + message.senderID + ".txt");
                fs.writeFileSync("./data/LastMSG/commandEx/" + message.senderID + ".txt", message.body, "utf-8");
            }
        }

        viewLog();
    });
});